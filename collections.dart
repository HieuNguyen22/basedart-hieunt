void main(List<String> args) {
  // Multiple All value by 2 of all list
  print("-- Multiple All value by 2 of all list");
  List<int> numList = [10, 20, 30, 40, 50];
  var doubleList = numList.map((value) => value * 2);

  print(doubleList);
  print("");

  // Combine Two Or More List In Dart
  print("-- Combine Two Or More List");
  List<String> names1 = ["Raj", "John", "Rocky"];
  List<String> names2 = ["Mike", "Subash", "Mark"];

  List<String> allNames1 = names1 + names2;
  List<String> allNames2 = [...names1, ...names2];
  print("Su dung + : $allNames1");
  print("Su dung ... : $allNames2");
  print("");

  // Conditions In List
  print("-- Conditions In List");
  bool isSad = true;
  var cart = ['milk', 'bread', 'egg', if (isSad) 'knife'];
  print("Cart: $cart");
  print("");

  // Where in List
  print("-- Where in list");
  List<int> numCheckEven = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  List<int> evenNums = numCheckEven.where((num) => num.isEven).toList();
  print(evenNums);
  print("");

  // Sets
  print("-- Set");
  Set<String> fruitsSet = {'Apple', 'Coconut', 'Lemon'};
  fruitsSet.add('Apple');
  fruitsSet.add('Grape');
  print(fruitsSet);
  print("");

  // Maps
  print("-- Map");
  Map<String, dynamic> book = {
    'title': 'Windy Hill',
    'author': 'Agatha Christine',
    'page': 333
  };

  // Loop through map
  for (MapEntry book in book.entries) {
    print('Key is ${book.key}. Value is ${book.value}');
  }
  print("");

  // Loop through for each
  book.forEach((key, value) => print('Key: $key. Value: $value'));
  print("");

  // Enhance Enumerations
  print("-- Enhance Enumerations");
  print(
      "The ${Water.frezing.name} point for water is ${Water.frezing.temperatureInF} degree F.");
  print(
      "The ${Water.boiling.name} point for water is ${Water.boiling.temperatureInF} degree F. ");
}

enum Water {
  frezing(32),
  boiling(212);

  final int temperatureInF;
  const Water(this.temperatureInF);
}
