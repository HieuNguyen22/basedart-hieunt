void main(List<String> args) {
  // Assert
  var age = 22;
  assert(age != 22);
  assert(age != 22, "Age is 22");

  // For Each
  print("-- In danh sach cau thu:");
  List<String> players = ['Messi', 'Ronaldo', 'Neymar', 'Suarez', 'Torres'];
  players.forEach((names) => print(names));
  print("");

  print("-- Tinh tong:");
  List<int> numbersForeach = [5,9,2,30];
  int total = 0;

  numbersForeach.forEach((num) => total += num);
  print("Ket qua: $total\n");

  // For in loop
  print("-- In danh sach cau thu su dung For In Loop");
  for (var player in players) {
    print(player);
  }
  print("");

  // Find index value of list
  print("In key - value");
  players.asMap().forEach((key, value) => print("Key: $key - Value: $value"));
  print("");
}