import 'dart:io';

void main() {
  // Lam tron
  double price = 5.2323232323232323;
  print("Before being rounded: $price");
  print("After being rounded: ${price.toStringAsFixed(2)}\n");

  // String nhieu dong
  String multiLineText = '''
  Thanh thien nhat doa van
  Giao chi nhat chi hoa
  Y! Van tan tuyet tan hoa tan nguyet khuyet.
  ''';
  print(multiLineText);

  // dung Raw truoc String
  String withoutRawString = "Toi yeu em \t !";
  String withRawString = r"Toi yeu em \t !";
  print(withoutRawString);
  print(withRawString + "\n");

  // Convert String to Int
  String strValue = "1";
  print("Type of strValue is: ${strValue.runtimeType}");
  int intValue = int.parse(strValue);
  print("Type of intValue is: ${intValue.runtimeType}");
  print("intValue: $intValue\n");

  // Convert Double to Int
  double numDouble = 10.3;
  int numInt = numDouble.toInt();
  print(
      "The value of numDouble is $numDouble. Its type is ${numDouble.runtimeType}");
  print("The value of numInt is $numInt. Its type is ${numInt.runtimeType}\n");

  // List
  List<String> names = ['Hieu', 'Nghia', 'Ha', 'Thanh'];
  for (int i = 0; i < names.length; i++) {
    print("Value of names[$i]: ${names[i]}. Its length is: ${names[i].length}");
  }
  print("\n");

  // Sets
  Set<String> weekday = {'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'};
  print(weekday);

  // Maps
  Map<String, String> myDetails = {
    'name': 'Hieu Nguyen',
    'age': '21',
    'address': 'Ha Dong',
    'nationality': 'Vietnamese'
  };

  print(myDetails);
  print(myDetails['name']);
  print('\n');

  // Runes
  String value = 'hieu';
  print("Unicode of value is: ${value.runes}\n");

  // Check Runtime Type
  var a = '10';
  var b = 10;

  print("Is type of a is Int?");
  if (!(a is int))
    print("No type of a is: ${a.runtimeType}\n");
  else
    print("Yes type of a is: ${a.runtimeType}\n");

  print("Is type of b is String?");
  if (b is! String)
    print("No type of b is: ${b.runtimeType}\n");
  else
    print("Yes type of b is: ${b.runtimeType}\n");

  /// Optionally Typed Language
  dynamic myVariable = 50;
  myVariable = 'Hello';
  print(myVariable + "\n");

  // String user input
  print("Enter your name:");
  String? name = stdin.readLineSync();
  print("The entered name is: $name \n");

  // Integer User input
  print("Enter your age:");
  int? age = int.parse(stdin.readLineSync()!);
  print("The entered age is: $age \n");

  // Floating Point user input
  print("Enter your weight:");
  double weight = double.parse(stdin.readLineSync()!);
  print("The entered weight is: $weight \n");


  // Properties Of String
  String strTest = 'Hi';
  print("Using codeUnits: ${strTest.codeUnits}");
  print("Using runes: ${strTest.runes}\n");

  // Replace String in Dart
  String originalText = 'May the force be with you.';
  String newText = originalText.replaceAll("you", 'me');

  print("Original text is: $originalText. New text is: $newText \n");

  // Reverse String in Dart
  String inputToReverse = 'Hello';
  print("Reversed text is: ${inputToReverse.split("").reversed.join()}");

}
