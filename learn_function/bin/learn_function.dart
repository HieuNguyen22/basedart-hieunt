import 'package:learn_function/learn_function.dart' as learn_function;

void main(List<String> arguments) {
  // Using default value
  print("-- Function using default value");
  printInfo("Hieu", "Male");
  printInfo("Giang", "Female", "Ms.");
  print("");

  // Using named parameter
  print("-- Function using named parameter");
  printAddress(name: "Hieu", address: "Hanoi");
  printAddress(address: "Saigon", name: "Giang");
  print("");

  // Using required named parameter
  print("-- Function using required named parameter");
  printPhoneNum(name: "Hieu", phoneNum: 377890362);
  printPhoneNum(phoneNum: 123456789, name: "Giang");
  print("");

  // Anonymous function
  print("-- Anonymous Function");
  const fruits = ['Apple', 'Mango', 'Banana', 'Orange'];

  fruits.forEach((fruit) {
    // Anonymous function
    print(fruit);
  });
  print("");

  // With arrow function
  print("With arrow function");
  calculateInterest(5000, 3, 3);
}

void printInfo(String name, String gender, [String title = "sir/myam"]) {
  print("Hello $title $name. Your gender is $gender");
}

void printAddress({String? name, String? address}) {
  print("Hello $name. Your addres is $address.");
}

void printPhoneNum({required String name, required int phoneNum}) {
  print("Hello $name. Your phone numver is $phoneNum.");
}

void calculateInterest(double principal, double rate, double time) =>
    print("Interest: ${principal * rate * time / 100}");
