import 'dart:math';

void main(List<String> args) {
  // Random
  print("-- Random");
  Random rand = new Random();
  print("Random number between 0-9: ${rand.nextInt(10)}");

  List<int> randomList = List.generate(10, (index) => rand.nextInt(100)+1);
  print("Random list between 1-100: $randomList");
}
